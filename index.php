<?php
$dir_path = "/site2";
$company['name'] = 'AnyModel';
?>
<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Компьютерный клуб "<?= $company['name'] ?>"</title>
    <meta name="description" content='Компьютерный клуб "<?= $company['name'] ?>" - лучшее, что будет в твоей жизни'>
    <link rel="stylesheet" href="<?= $dir_path ?>/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= $dir_path ?>/assets\css\slick.css">
    <link rel="stylesheet" href="<?= $dir_path ?>/assets\css\slick-theme.css">
    <link rel="stylesheet" href="<?= $dir_path ?>/assets/css/main.css">
</head>

<body>
    <header class="w-100 position-absolute close">
        <nav class="navbar navbar-expand-lg navbar-dark bg-transparent">
            <div class="container-fluid d-flex justify-content-around">
                <a class="navbar-brand" href="<?= $dir_path ?>/"><img class="logo" src="<?= $dir_path ?>/assets/img/logo.png" alt="<?= $company['name'] ?>"></a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-end" id="navbarNavAltMarkup">
                    <ul class="navbar-nav">
                        <li><a class="nav-link active" aria-current="page" href="<?= $dir_path ?>/">Главная</a></li>
                        <li><a class="nav-link" href="<?= $dir_path ?>/price">Прайс-лист</a></li>
                        <li><a class="nav-link" href="<?= $dir_path ?>/contacts">Контакты</a></li>
                        <li><a class="nav-link" href="<?= $dir_path ?>/about">О нашем клубе</a></li>
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <section id="welcome" class="d-flex flex-column justify-content-center align-items-center">
        <div class="welcome__about text-center">
            <h1 class="text-uppercase fw-bold fst-italic"><?= $company['name'] ?></h1>
            <h2>Лучший компьютерный клуб твоего города</h2>
        </div>
    </section>
    <main>
    <div class="container py-3">
        <div class="d-flex flex-column">
            <p class="text-uppercase fw-bold px-5">Наши партнёры</p>
            <div class="slider">
                <div class="text-center">
                    <a class="fw-bold text-decoration-none text-light" href="#">Партнёр №1</a>
                </div>
                <div class="text-center">
                    <a class="fw-bold text-decoration-none text-light" href="#">Партнёр №2</a>
                </div>
                <div class="text-center">
                    <a class="fw-bold text-decoration-none text-light" href="#">Партнёр №3</a>
                </div>
            </div>
        </div>
    </div>
    </main>
    <footer class="bg-light">
        <div>
            <div class="list-group-item">Время работы</div>
            <div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item py-1"><span>Пн, ср, пт</span>: <span>8:00 - 23:00</span></li>
                    <li class="list-group-item py-1"><span>Вт, чт</span>: <span>8:00 - 23:00</span></li>
                    <li class="list-group-item py-1"><span>Сб, вс</span>: <span>8:00 - 23:00</span></li>
                </ul>
            </div>
        </div>
    </footer>
    <script src="<?= $dir_path ?>/assets/js/bootstrap.bundle.min.js"></script>
    <script src="<?= $dir_path ?>/assets/js/jquery-3.6.0.min.js"></script>
    <script src="<?= $dir_path ?>/assets/js/slick.min.js"></script>
    <script src="<?= $dir_path ?>/assets/js/main.js"></script>
</body>

</html>
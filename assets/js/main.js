$(document).ready(function(){
    const welcome = $('#welcome');
    if (welcome.length === 1) {
        
        setTimeout(() => {
            welcome.css('opacity', 1);
            $('header').css('opacity', 1);
            setTimeout(() => {welcome.find('h2').animate({top: '-5vh'});}, 0.5 * 1000);
            setTimeout(() => {welcome.find('h1').animate({top: '-5vh'});}, 0.7 * 1000);
        }, 1 * 1000);

    }
});

$('.slider').slick({
    autoplay: true,
    infinite: true,
    centerMode: true,
    slidesToShow: 3,
    slidesToScroll: 1
});